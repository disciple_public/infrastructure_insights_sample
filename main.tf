terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  region = "eu-west-2"
}

resource "aws_vpc" "my_vpc" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Name = "tf-example"
  }
}

resource "aws_subnet" "my_subnet" {
  vpc_id = aws_vpc.my_vpc.id
  cidr_block = "172.16.10.0/24"
  availability_zone = "eu-west-2a"

  tags = {
    Name = "tf-example"
  }
}

resource "aws_network_interface" "foo" {
  subnet_id = aws_subnet.my_subnet.id
  private_ips = [
    "172.16.10.100"]

  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_instance" "foo" {
  ami = "ami-005e54dee72cc1d00"
  # us-west-2
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.foo.id
    device_index = 0
  }

  credit_specification {
    cpu_credits = "unlimited"
  }
}

resource "aws_default_network_acl" "default" {
  default_network_acl_id = aws_vpc.my_vpc.default_network_acl_id

  egress {
    protocol = -1
    rule_no = 100
    action = "allow"
    cidr_block = "0.0.0.0/0"
    from_port = 0
    to_port = 0
  }

}

resource "aws_network_acl" "allow_ssh" {
  vpc_id = aws_vpc.my_vpc.id

  egress {
    protocol = "tcp"
    rule_no = 101
    action = "allow"
    cidr_block = "0.0.0.0/0"
    from_port = 23
    to_port = 23
  }

  ingress {
    protocol = "tcp"
    rule_no = 100
    action = "allow"
    cidr_block = "0.0.0.0/0"
    from_port = 23
    to_port = 23
  }
}
